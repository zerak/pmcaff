## 云信服务端接入使用说明

#### 云信帐号的创建 [登录流程图](https://gitlab.com/zerak/pmcaff/raw/master/login.jpg "登录流程图")

`im/getToken`

| 字段 | 类型 | 是否必需 | 举例值 |
| ------ | ------ | ------ | ------ | 
| uid | int | 是 | 用户登录标识 |

```
{
	"accid": 1234,    // 云信accid，同用户系统用户ID
	"token":"xxxx",   // 云信客户端登录token
}
```

`需要PMCaff服务器自己存储现有用户系统与云信帐号的映射关系，具体可参见数据库表`[im_user](https://gitlab.com/zerak/pmcaff/blob/master/doc/sql.sql "im_user")

`为了与原有用户系统的平滑接入，云信帐号的创建，依据惰性创建原则，即仅在客户端获取云信登录Token时，有则使用，无则创建。`

`云信Token的返回，可以与现有用户登录的接口同步返回，也可以在客户端登录成功后，单独获取云信Token`


#### 群组的创建
`im/createTeam`

| 字段 | 类型 | 是否必需 | 举例值 |
| ------ | ------ | ------ | ------ | 
| uid | int | 是 | 用户登录标识 |
| tname | string | 是 | 群聊名称 |
| avatar | string | 是 | 群聊头像 | 
| type  | string | 否 | 群聊类型 0(默认)开放群聊、1普通群聊、2私密群 | 
| limit | int | 否 | 默认群聊人数 200(默认)/500 |

成功返回数据：
```
{
	"tid": "12345678",    // 群聊ID
}
```
错误返回数据(仅示例,错误码参见[云信文档](https://dev.yunxin.163.com/docs/product/IM%E5%8D%B3%E6%97%B6%E9%80%9A%E8%AE%AF/%E6%9C%8D%E5%8A%A1%E7%AB%AFAPI%E6%96%87%E6%A1%A3/code%E7%8A%B6%E6%80%81%E8%A1%A8)):
```
{
	"code": 414,
}
```