
CREATE TABLE `im_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `accid` varchar(64) NOT NULL DEFAULT '',
  `token` varchar(64) NOT NULL DEFAULT '',
  `addtime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_uid` (`user_id`),
  UNIQUE KEY `uk_accid` (`accid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `im_team` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` varchar(64) NOT NULL DEFAULT '' COMMENT '群聊ID',
  `tname` varchar(64) NOT NULL COMMENT '群名',
  `owner` int(11) NOT NULL,
  `accid` varchar(64) NOT NULL DEFAULT '',
  `type` int(1) NOT NULL DEFAULT '0' COMMENT '0开放群聊 1普通群聊 2私密群',
  `avatar` varchar(128) NOT NULL DEFAULT '0' COMMENT '头像',
  `custom` varchar(256) NOT NULL DEFAULT '' COMMENT '自定义Json结构体 bg:http:""',
  `user_limit` int(4) NOT NULL DEFAULT '0' COMMENT '用户数',
  `is_pay` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_delete` enum('Y','N') NOT NULL DEFAULT 'N',
  `addtime` datetime NOT NULL,
  `modtime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_tid` (`tid`),
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;