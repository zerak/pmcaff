<?php

class YunXinIM {
    const TIME_OUT = 1;

    // NOTE appKey/appSecret 正式与测试环境区别开
    const APP_KEY = "d23b8d6ae46c68c0d22899f9df9c135a";
    const APP_SECRET = "d47468c3f8c5";

    // YunXin IM urls
    const BASE_URL      = "https://api.netease.im/nimserver/";
    // user
    const CREATE_USER       = self::BASE_URL . "user/create.action";            // 创建帐号
    const UPDATE_USER       = self::BASE_URL . "user/updateUinfo.action";       // 更新用户信息
    const GET_USERS_INFO    = self::BASE_URL . "user/getUinfos.action";         // 更新用户信息

    // team
    const CREATE_TEAM = self::BASE_URL . "team/create.action";    // 创建群聊
    const UPDATE_TEAM = self::BASE_URL . "team/update.action";    // 更新群信息
    const QUERY_TEAM  = self::BASE_URL . "team/query.action";     // 群信息与成员列表查询

    /***
     * header 设置规则
     * https://dev.yunxin.163.com/docs/product/IM%E5%8D%B3%E6%97%B6%E9%80%9A%E8%AE%AF/%E6%9C%8D%E5%8A%A1%E7%AB%AFAPI%E6%96%87%E6%A1%A3/%E6%8E%A5%E5%8F%A3%E6%A6%82%E8%BF%B0
     */
    private static function post($url, $arr) {
        $nonce = time();
        $curTime = time();
        $checkSum = sha1(self::APP_SECRET . $nonce . $curTime); 
        $params = array();
        foreach($arr as $k => $v) {
            $params[$k] = $v;
        }
        $content = http_build_query($params);
        $options = array(
            "http" => array(
                "method" => "POST",
                "header"  => "Content-type: application/x-www-form-urlencoded\r\n".
                  "AppKey: ". self::APP_KEY. "\r\n". 
                  "Nonce: " . $nonce. "\r\n".
                  "CurTime: " . $curTime. "\r\n".
                  "CheckSum: ". $checkSum . "\r\n",
                "timeout" => self::TIME_OUT,   // NOTE request time out
                "content" => $content,
            ),
        );
        $context  = stream_context_create($options); 
        try {
            $result = file_get_contents($url, false, $context);
            if($result == flase) {
                // request error, return empy array
                return array();
            } else {
                return json_decode($result, true);
            }
        } catch(Exception $err) {
            // NOTE trow error
            // throw new RuntimeException();
        }
    }

    /***
     * 创建网易云通信ID
     * https://dev.yunxin.163.com/docs/product/IM%E5%8D%B3%E6%97%B6%E9%80%9A%E8%AE%AF/%E6%9C%8D%E5%8A%A1%E7%AB%AFAPI%E6%96%87%E6%A1%A3/%E7%BD%91%E6%98%93%E4%BA%91%E9%80%9A%E4%BF%A1ID?#%E5%88%9B%E5%BB%BA%E7%BD%91%E6%98%93%E4%BA%91%E9%80%9A%E4%BF%A1ID
     * @accid 用户ID,为开发维护方便，网易云通信ID与用户ID一一对应
     * @return {
                "code":200,
                "info":{"token":"xx","accid":"xx","name":"xx"} 
            }
     */
    public static function createUser($accid, $name, $avatar, $token, $extend) {
        $params = array(
            "accid" => $accid,
            "name" => $name,
            "icon" => $avatar,
            "token" => $token,
            "ex" => $extend,
        );
        return self::post(self::CREATE_USER, $params);
    }

    /***
     * @accid 用户ID
     * @fields array,key可为如下值
     * name,icon,sign,email,birth,mobile,gender,ex
     * @return
     */
    public static function updateUser($accid, $fields) {
        $fields["accid"] = $accid;
        return self::post(self::UPDATE_USER, $fields);
    }

    /***
     * @accids must be array
     */
    public static function getUsersInfo($accids) {
        $params = array(
            "accids" => json_encode(is_array($accids) ? $accids : (array)(int)$accids),
        );
        return self::post(self::GET_USERS_INFO, $params);
    }

    /**
     * 创建群聊
     * https://dev.yunxin.163.com/docs/product/IM%E5%8D%B3%E6%97%B6%E9%80%9A%E8%AE%AF/%E6%9C%8D%E5%8A%A1%E7%AB%AFAPI%E6%96%87%E6%A1%A3/%E7%BE%A4%E7%BB%84%E5%8A%9F%E8%83%BD%EF%BC%88%E9%AB%98%E7%BA%A7%E7%BE%A4%EF%BC%89?#%E5%88%9B%E5%BB%BA%E7%BE%A4
     * @owner 用户ID
     * @tpe 群聊属性 0不用验证 1需要验证 2不允许任何人加入
     * @tname 群聊名字
     * @avatar 群聊头像
     * @custom 自定义字段
     * @limit 群最大人数
     */
    public static function createTeam($owner, $tpe, $tname, $avatar, $custom, $limit) {
        $params = array(
            "owner" => $owner,
            "tname" => $tname,
            "members" => json_encode(array($owner. "")),
            "msg" => $tname,
            "magree" => 1,
            "joinmode" => $tpe,
            "custom" => $custom,
            "icon" => $avatar,
            "beinvitemode" => "0",
            "invitemode", "0",
            "uptinfomode", "0",
            "upcustommode", "0",
            "teamMemberLimit", "" . $limit,
        );
        return self::post(self::CREATE_TEAM, $params);
    }

    /***
     * @owner 群主用户ID
     * @tid 群聊ID
     * @fields 为以下任何Key:Val值
     * tname	        群名称，最大长度64字符
     * announcement	    群公告，最大长度1024字符
     * intro	        群描述，最大长度512字符
     * joinmode	        群建好后，sdk操作时，0不用验证，1需要验证,2不允许任何人加入。其它返回414
     * custom	        自定义高级群扩展属性，第三方可以跟据此属性自定义扩展自己的群属性。（建议为json）,最大长度1024字符
     * icon	            群头像，最大长度1024字符
     * beinvitemode	    被邀请人同意方式，0-需要同意(默认),1-不需要同意。其它返回414
     * invitemode	    谁可以邀请他人入群，0-管理员(默认),1-所有人。其它返回414
     * uptinfomode	    谁可以修改群资料，0-管理员(默认),1-所有人。其它返回414
     * upcustommode	    谁可以更新群自定义属性，0-管理员(默认),1-所有人。其它返回414
     * teamMemberLimit	该群最大人数(包含群主)，范围：2至应用定义的最大群人数(默认:200)。其它返回414
     */
    public static function updateTeam($owner, $tid, $fields) {
        $params = array(
            "owner" => $owner,
            "tid" => $tid,
        ); 
        foreach($fields as $k => $v) {
            $params[$k] = $v;
        }
        return self::post(self::UPDATE_TEAM, $params);
    }

    /***
     * @tids array(tid1,tid2)
     * @op 0/1 是否带成员信息
     * NOTE 接口有请求限制
     * https://dev.yunxin.163.com/docs/product/IM%E5%8D%B3%E6%97%B6%E9%80%9A%E8%AE%AF/%E6%9C%8D%E5%8A%A1%E7%AB%AFAPI%E6%96%87%E6%A1%A3/%E7%BE%A4%E7%BB%84%E5%8A%9F%E8%83%BD%EF%BC%88%E9%AB%98%E7%BA%A7%E7%BE%A4%EF%BC%89?#%E8%8E%B7%E5%8F%96%E7%BE%A4%E7%BB%84%E8%AF%A6%E7%BB%86%E4%BF%A1%E6%81%AF
     */
    public static function getTeamsInfo($tids, $op) {
        $params = array(
            "tids" => json_encode(is_array($tids) ? $tids: (array)(int)$tids),
            "ope" => $op,
        );
        return self::post(self::QUERY_TEAM, $params);
    }

}

/*
// create user
var_dump(YunXinIM::createUser(2, "testUserName2", "https://baidu.com","",""));
$user = array(
    "2",
);
var_dump(YunXinIM::getUsersInfo($user));

$user = array(
    2,
);
var_dump(YunXinIM::getUsersInfo($user));

$user = 2;
var_dump(YunXinIM::getUsersInfo($user));

$user = "2";
var_dump(YunXinIM::getUsersInfo($user));

// update
$userInfo = array(
    "name" => "testUserName22"
);
var_dump(YunXinIM::updateUser(2, $userInfo));

// get users info
$user = array(
    "1",
    2,
);
var_dump(YunXinIM::getUsersInfo($user));

// create team
$teamInfo = YunXinIM::createTeam(1, 0, "teatTname", "https://baidu.com", "custom", "200");
var_dump($teamInfo);
$tid = array($teamInfo["tid"]);
var_dump(YunXinIM::getTeamsInfo($tid, 1));
*/

/*
2559483267
2559461689
*/
$tids = 2559483267;
$tinfo = array(
    "tname" => "new tname",
);
var_dump(YunXinIM::updateTeam(1, $tids, $tinfo));
var_dump(YunXinIM::getTeamsInfo($tids, 1));